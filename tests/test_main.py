import unittest
import pandas as pd
from main import InD_transform_data, Location, Time, ObjectClass, Record, TrackMeta, Trajectory_point

class TestInDTransformData(unittest.TestCase):
    def test_transform_data(self):
        raw_data_recordings_meta = pd.DataFrame({
            'recordingId': [1, 2, 3],
            'locationId': [101, 102, 103],
            'frameRate': [25, 25, 25],
            'speedLimit': [50,75,90],
            'latLocation': [40.7128, 34.0522, 37.7749],
            'lonLocation': [-74.0060, -118.2437, -122.4194],
            'xUtmOrigin': [0, 100, 200],
            'yUtmOrigin': [0, 200, 400],
            'orthoPxToMeter': [0.1, 0.2, 0.3],
            'weekday': ['Monday', 'Tuesday', 'Wednesday'],
            'startTime': ['09:00', '10:00', '11:00'],
            'duration': [3600, 3600, 3600],
            'numTracks': [2, 3, 4],
            'numVehicles': [1, 2, 3],
            'numVRUs': [1, 1, 1],

        })

        raw_data_tracks_meta = pd.DataFrame({
            'recordingId': [1, 1, 2, 2, 3, 3],
            'trackId': [1, 2, 3, 4, 5, 6],
            'initialFrame': [0, 10, 0, 5, 0, 15],
            'finalFrame': [9, 20, 4, 15, 14, 25],
            'numFrames': [10, 11, 5, 11, 15, 11],
            'tracked_object_width': [1.5, 2.0, 1.2, 1.8, 1.6, 1.9],
            'tracked_object_length': [3.0, 4.0, 2.5, 3.5, 3.2, 3.8],
            'class': ['car', 'car', 'pedestrian', 'pedestrian', 'car', 'car']
        })

        raw_data_tracks = pd.DataFrame({
            'recordingId': [1, 1, 1, 2, 2, 2, 3, 3, 3],
            'trackId': [1, 2, 3, 3, 4, 5, 5, 6, 7],
            'frame': [0, 1, 2, 0, 1, 2, 0, 1, 2],
            'trackLifetime': [10, 10, 10, 15, 15, 15, 20, 20, 20],
            'xCenter': [0.0, 1.0, 2.0, 0.0, 1.0, 2.0, 0.0, 1.0, 2.0],
            'yCenter': [0.0, 1.0, 2.0, 0.0, 1.0, 2.0, 0.0, 1.0, 2.0],
            'heading': [0.0, 0.1, 0.2, 0.0, 0.1, 0.2, 0.0, 0.1, 0.2],
            'object_width': [1.5, 1.5, 1.5, 1.2, 1.2, 1.2, 1.6, 1.6, 1.6],
            'object_length': [3.0, 3.0, 3.0, 2.5, 2.5, 2.5, 3.2, 3.2, 3.2],
            'xVelocity': [0.0, 0.1, 0.2, 0.0, 0.1, 0.2, 0.0, 0.1, 0.2],
            'yVelocity': [0.0, 0.1, 0.2, 0.0, 0.1, 0.2, 0.0, 0.1, 0.2],
            'xAcceleration': [0.0, 0.1, 0.2, 0.0, 0.1, 0.2, 0.0, 0.1, 0.2],
            'yAcceleration': [0.0, 0.1, 0.2, 0.0, 0.1, 0.2, 0.0, 0.1, 0.2],
            'lonVelocity': [0.0, 0.1, 0.2, 0.0, 0.1, 0.2, 0.0, 0.1, 0.2],
            'latVelocity': [0.0, 0.1, 0.2, 0.0, 0.1, 0.2, 0.0, 0.1, 0.2],
            'lonAcceleration': [0.0, 0.1, 0.2, 0.0, 0.1, 0.2, 0.0, 0.1, 0.2],
            'latAcceleration': [0.0, 0.1, 0.2, 0.0, 0.1, 0.2, 0.0, 0.1, 0.2]
        })

        locations_list, times_list, object_classes_list, records_list, tracks_meta_list, trajectory_points_list = InD_transform_data(
            raw_data_recordings_meta, raw_data_tracks_meta, raw_data_tracks)

        # Assert the length of the lists
        self.assertEqual(len(locations_list), 3)
        self.assertEqual(len(times_list), 3)
        self.assertEqual(len(object_classes_list), 6)
        self.assertEqual(len(records_list), 6)
        self.assertEqual(len(tracks_meta_list), 6)
        self.assertEqual(len(trajectory_points_list), 6)

        # Assert the types of the objects in the lists
        self.assertIsInstance(locations_list[0], Location)
        self.assertIsInstance(times_list[0], Time)
        self.assertIsInstance(object_classes_list[0], ObjectClass)
        self.assertIsInstance(records_list[0], Record)
        self.assertIsInstance(tracks_meta_list[0], TrackMeta)
        self.assertIsInstance(trajectory_points_list[0], Trajectory_point)

if __name__ == '__main__':
    unittest.main()