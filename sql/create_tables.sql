CREATE TABLE "trajectory" (
  "id" SERIAL PRIMARY KEY,
  "trajectory_id" integer,
  "tracked_object_id" integer,
  "location_id" integer,
  "trajectory_start_time_id" integer,
  "trajectory_end_time_id" integer,
  "trajectory_start_date_id" integer,
  "trajectory_end_date_id" integer,
  "weather_condition_id" integer,
  "record_id" integer,
  "initial_frame" integer,
  "final_frame" integer,
  "num_frames" integer,
  "duration" real,
  "distance_travelled" real,
  "average_velocity" real,
  "max_velocity" real,
  "min_velocity" real,
  "imported_at" timestamp
);

CREATE TABLE "location" (
  "id" SERIAL PRIMARY KEY,
  "location_id" integer,
  "speedLimit" real,
  "latitude" real,
  "longitude" real,
  "imported_at" timestamp
);

CREATE TABLE "tracked_object" (
  "id" SERIAL PRIMARY KEY,
  "width" real,
  "length" real,
  "class" varchar,
  "imported_at" timestamp
);

CREATE TABLE "trajectory_start_time" (
  "id" SERIAL PRIMARY KEY,
  "hour" integer,
  "minute" integer,
  "second" real,
  "imported_at" timestamp
);

CREATE TABLE "trajectory_end_time" (
  "id" SERIAL PRIMARY KEY,
  "hour" integer,
  "minute" integer,
  "second" real,
  "imported_at" timestamp
);

CREATE TABLE "trajectory_start_date" (
  "id" SERIAL PRIMARY KEY,
  "weekday" varchar,
  "day" integer,
  "week" integer,
  "month" integer,
  "year" integer,
  "imported_at" timestamp
);

CREATE TABLE "trajectory_end_date" (
  "id" SERIAL PRIMARY KEY,
  "weekday" varchar,
  "day" integer,
  "week" integer,
  "month" integer,
  "year" integer,
  "imported_at" timestamp
);

CREATE TABLE "record" (
  "id" SERIAL PRIMARY KEY,
  "record_id" integer,
  "framerate" integer,
  "duration" real,
  "xUtmOrigin" real,
  "yUtmOrigin" real,
  "orthoPxToMeter" real,
  "num_tracks" integer,
  "num_vehicules" integer,
  "num_vrus" integer,
  "imported_at" timestamp
);

CREATE TABLE "weather_condition" (
  "id" SERIAL PRIMARY KEY,
  "city_name" varchar(255),
  "lat" real,
  "long" real,
  "dt" integer,
  "sunrise" integer,
  "sunset" integer,
  "temp" real,
  "feels_like" real,
  "pressure" integer,
  "humidity" integer,
  "dew_point" real,
  "uvi" real,
  "clouds" integer,
  "visibility" integer,
  "wind_speed" real,
  "wind_deg" real,
  "weather_id" integer,
  "weather_main" varchar(255),
  "weather_description" varchar(255),
  "weather_icon" varchar(255),
  "imported_at" timestamp
);

ALTER TABLE "trajectory" ADD FOREIGN KEY ("location_id") REFERENCES "location" ("id");

ALTER TABLE "trajectory" ADD FOREIGN KEY ("tracked_object_id") REFERENCES "tracked_object" ("id");

ALTER TABLE "trajectory" ADD FOREIGN KEY ("trajectory_start_time_id") REFERENCES "trajectory_start_time" ("id");

ALTER TABLE "trajectory" ADD FOREIGN KEY ("trajectory_end_time_id") REFERENCES "trajectory_end_time" ("id");

ALTER TABLE "trajectory" ADD FOREIGN KEY ("trajectory_start_date_id") REFERENCES "trajectory_start_date" ("id");

ALTER TABLE "trajectory" ADD FOREIGN KEY ("trajectory_end_date_id") REFERENCES "trajectory_end_date" ("id");

ALTER TABLE "trajectory" ADD FOREIGN KEY ("record_id") REFERENCES "record" ("id");

ALTER TABLE "trajectory" ADD FOREIGN KEY ("weather_condition_id") REFERENCES "weather_condition" ("id");
