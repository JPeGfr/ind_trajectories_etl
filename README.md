# InD dataset ETL for Trajectories Data Warehouse

English version of this document is available [below](#ind-dataset-etl-for-trajectories-data-warehouse-english-version).

Ensemble de scripts Python pour extraire, transformer et charger les données du jeu de données InD  [https://levelxdata.com/ind-dataset/](https://levelxdata.com/ind-dataset/) dans le Data Warehouse des trajectoires.

Le fichier `main.py` contient le code principal de l'application. 
Son objectif est de créer une chaine de traitement de données qui permet de lire les données d'entrée, de les traiter et de les charger dans le Data Warehouse.

Les données d'entrée sont des données de jeu de données InD.
Les données de sortie sont des données de jeu de données InD transformées pour être compatibles avec le modèle du Data Warehouse.

Les données d'entrée sont lues à partir de fichiers CSV. Ces fichiers sont stockés dans le répertoire défini dans le fichier de configuration `.env`.

Un exemple de fichier de configuration est fourni dans le fichier `example.env`.

Ce logiciel utilise l'environnement virtuel Python. Pour plus d'informations sur l'environnement virtuel Python, voir [https://docs.python.org/fr/3/tutorial/venv.html](https://docs.python.org/fr/3/tutorial/venv.html) ainsi que le locigiel docker-compose. Pour plus d'informations sur docker-compose, voir [https://docs.docker.com/compose/](https://docs.docker.com/compose/).

## Quelques explications

### Fonctionnalités principales

Le code dans `main.py` implémente les fonctionnalités suivantes :

1. Lecture des données d'entrée : Le programme lit les données d'entrée à partir de fichiers csv.

2. Traitement des données : Les données lues sont traitées et transformées selon le modèle défini dans le data warehouse.

3. Chargement des données : Les données transformées sont chargées dans le data warehouse.

### Architecture

Le code est organisé selon l'architecture suivante :

- `main.py` : Code principal de l'application.
- `sql` : Répertoire contenant les scripts sql de création des tables du data warehouse.
- `src` : Répertoire contenant les modules python de l'application.
    - `orm.py` : Module contenant les fonctions de connexion à la base de données.
    - `X_etl.py` : Module contenant les fonctions de l'ETL (il y a un fichier `etl.py` par type de source. Exemple `InD_etl.py`).
    - `logger.py` : Module contenant les fonctions de journalisation (WIP).

La couche d'accès aux données est implémentée avec SQLAlchemy. Pour plus d'informations sur SQLAlchemy, voir [https://www.sqlalchemy.org/](https://www.sqlalchemy.org/).

Ainsi, la couche d'accès aux données est indépendante du type de base de données utilisée. Il est possible de changer de base de données sans modifier le code de l'application. Chaque table du data warehouse est représentée par une classe Python. Ces classes sont définies dans le fichier `orm.py`.

Chaques sources de données possède son propre module ETL. Par exemple, le module `InD_etl.py` implémente les fonctions de l'ETL pour les données du jeu de données InD.

### Modèle du data warehouse

Le modèle du data warehouse est défini dans le fichier `sql/create_tables.sql`.

Le modèle du data warehouse est le suivant :

![Modèle du data warehouse](./img/TrajectoryDW.png)

Le modèle du data warehouse est composé de 7 tables en étoile :

- `trajectory` : Table des faits des trajectoires.
- `location` : Table de dimension de localisation des points de mesure.
- `trajectory_start_time` : Table de dimension du temps de début de la trajectoire.
- `trajectory_end_time` : Table de dimension du temps de fin de la trajectoire.
- `trajectory_start_date` : Table de dimension de la date de début de la trajectoire.
- `trajectory_end_date` : Table de dimension de la date de fin de la trajectoire.
- `tracked_object` : Table de dimension des objets suivis.
- `record` : Table de dimension des enregistrements.
- `weather_condition` : Table de dimension des conditions météorologiques (les données du dataset InD n'étant pas horadatées il n'est pas possible d'associer les conditions météo aux trajectoires issues de ce dataset).

## Configuration

### Prérequis

Le fichier de configuration `.env` permet de configurer le répertoire de travail et les paramètres de la base de données.

Le fichier `example.env` contient un exemple de fichier de configuration.

Il faut créer un fichier `.env` à partir du fichier `example.env` et le configurer selon les besoins.

Créer ensuite un répertoire et y placer les fichiers csv contenant les données d'entrée. Ce répertoire doit être configuré dans le fichier `.env`.

Il faut configurer l'accès à la base de données PostgreSQL en spécifiant les paramètres de connexion dans le fichier `.env`.

### Création de l'environnement virtuel

Créer l'environnement virtuel du projet en exécutant la commande suivante :

```bash
python -m venv venv
```

Activer l'environnement virtuel en exécutant la commande suivante :

```bash
source venv/bin/activate
```

Enfin, avant de lancer le programme, il faut installer les dépendances du projet en exécutant la commande suivante :

```bash
pip install -r requirements.txt
```

### Création de la base de données

Pour créer la base de données, il faut se connecter au conteneur Docker de la base de données en exécutant la commande suivante :

```bash
docker exec -it TrajectoriesDW_DB psql -U postgres
```

`TrajectoriesDW_DB` est le nom du conteneur Docker de la base de données. Cette information est définie dans le fichier `docker-compose.yml`.

Ensuite il faut créer la base de données en exécutant la commande suivante :

```bash
postgres=# CREATE DATABASE inddataset;
CREATE DATABASE
```

On crée ensuite l'utilisateur `inddataset` et on lui donne les droits sur la base de données `inddataset` :

```bash
postgres=# CREATE USER inddataset WITH PASSWORD 'inddataset';
CREATE ROLE
postgres=# GRANT ALL PRIVILEGES ON DATABASE inddataset TO inddataset;
GRANT
postgres=# ALTER DATABASE inddataset OWNER TO inddataset; 
ALTER DATABASE
```

Pour chaque table il faut donner les droits à l'utilisateur `inddataset` :

```bash
postgres=# GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO inddataset;
GRANT
postgres=# GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO inddataset;
GRANT
postgres=# GRANT ALL ON ALL TABLES IN SCHEMA public TO inddataset;
GRANT
postgres=# GRANT ALL ON ALL SEQUENCES IN SCHEMA public TO inddataset;
GRANT
```

Si celà ne suffit pour chaque table, on peut ajouter des droits spécifique sur la table `location` par exemple :
    
```bash
postgres=# GRANT ALL ON TABLE public.location TO inddataset;
GRANT
...
```

Remplacer `inddataset` par le nom de la base de données, de l'utilisateur et le mot de passe de la base de données configurés dans le fichier `.env` (`DB_NAME`, `DB_USER` et `DB_PASSWARD`).

le commande `\l` permet de lister les bases de données :

```bash
postgres=# \l
                                                          List of databases
    Name    |   Owner    | Encoding | Locale Provider |  Collate   |   Ctype    | ICU Locale | ICU Rules |     Access privileges     
------------+------------+----------+-----------------+------------+------------+------------+-----------+---------------------------
 inddataset | inddataset | UTF8     | libc            | en_US.utf8 | en_US.utf8 |            |           | =Tc/inddataset           +
            |            |          |                 |            |            |            |           | inddataset=CTc/inddataset
 postgres   | postgres   | UTF8     | libc            | en_US.utf8 | en_US.utf8 |            |           | 
 template0  | postgres   | UTF8     | libc            | en_US.utf8 | en_US.utf8 |            |           | =c/postgres              +
            |            |          |                 |            |            |            |           | postgres=CTc/postgres
 template1  | postgres   | UTF8     | libc            | en_US.utf8 | en_US.utf8 |            |           | =c/postgres              +
            |            |          |                 |            |            |            |           | postgres=CTc/postgres
(4 rows)
```

Quitter le prompt de la base de données en exécutant la commande suivante :

```bash
postgres=# \q
```

### Création des tables

Avant de lancer le programme, il faut créer les tables du data warehouse en exécutant le script `create_tables.sql` situé dans le répertoire `sql`.

Pour lancer le script sql dans le conteneur, taper la commande suivante :

```bash
docker exec -i TrajectoriesDW_DB psql -U postgres -d inddataset < ./sql/create_tables.sql
```

## Utilisation

Copier les fichiers csv contenant les données d'entrée dans le répertoire configuré dans le fichier `.env`.
Les fichiers csv doivent respecter le format du dataset InD. Plus d'informations sur le dataset InD sont disponibles sur le site [https://levelxdata.com/ind-dataset/](https://levelxdata.com/ind-dataset/).

Pour exécuter le code dans `main.py`, vous pouvez utiliser la commande suivante :

```bash
python main.py
```

## Tests unitaires

Les tests unitaires sont implémentés dans le fichier `test_main.py`.

Pour exécuter les tests unitaires, vous pouvez utiliser la commande suivante :

```bash 
python -m unittest test_main.py
```
## Exploitation des données du data warehouse

Pour exploiter les données du data warehouse, il faut utiliser un outil de requêtage SQL. Avec une base de données PostgreSQL, on peut utiliser l'outil pgAdmin. Pour plus d'informations sur pgAdmin, voir [https://www.pgadmin.org/](https://www.pgadmin.org/).

Pour exploiter les données du data warehouse, on peut par exemple lancer la requête SQL suivante :

```sql
SELECT
    tracked_object."class" AS class,
    trajectory_start_date.weekday AS day_of_week,
    "location"."speedLimit" AS speed_limit,
    COUNT(Trajectory.id) AS total_trajectories
FROM
    tracked_object 
JOIN
    trajectory Trajectory ON tracked_object.id = Trajectory.tracked_object_id
JOIN
    trajectory_start_date ON Trajectory.trajectory_start_date_id = trajectory_start_date.id
JOIN
    "location" ON Trajectory.location_id = "location".id
GROUP BY
    GROUPING SETS ((tracked_object."class", trajectory_start_date.weekday, "location"."speedLimit"), 
                   (tracked_object."class", trajectory_start_date.weekday), 
                   (tracked_object."class"), 
                   (trajectory_start_date.weekday), 
                   ())
ORDER BY
    tracked_object."class", trajectory_start_date.weekday, "location"."speedLimit";
```

Cette requête permet de compter le nombre de trajectoires par jour de la semaine, par classe d'objet suivi et par limite de vitesse.

Pour plus d'informations sur les requêtes SQL, voir [https://sql.sh/](https://sql.sh/).

## Licence

Ce logiciel et tous ses codes sources sont sous licence CeCILL v2.1. Pour plus d'informations sur la licence CeCILL v2.1, voir [https://cecill.info/licences/Licence_CeCILL_V2.1-fr.html](https://cecill.info/licences/Licence_CeCILL_V2.1-fr.html).

## Auteurs

- Jean-Paul Garrigos <jean-paul.garrigos@cerema.fr>

# InD dataset ETL for Trajectories Data Warehouse (English version)

Set of Python scripts for extracting, transforming and loading data from the InD dataset [https://levelxdata.com/ind-dataset/](https://levelxdata.com/ind-dataset/) into the Trajectory Data Warehouse.

The `main.py` file contains the application's main code. 
Its purpose is to create a data processing chain that reads input data, processes it and loads it into the data warehouse.

Input data is InD dataset data.
Output data is InD dataset data, transformed to be compatible with the data warehouse model.

Input data is read from CSV files. These files are stored in the directory defined in the `.env` configuration file.

An example configuration file is provided in the `example.env` file.

This software uses the Python virtual environment. For more information on the Python virtual environment, see [https://docs.python.org/fr/3/tutorial/venv.html](https://docs.python.org/fr/3/tutorial/venv.html) and the docker-compose locigiel. For more information on docker-compose, see [https://docs.docker.com/compose/](https://docs.docker.com/compose/).

## Some explanations

### Main features

The code in `main.py` implements the following functions:

1. Reading input data: The program reads input data from csv files.

2. Data processing : Read data is processed and transformed according to the model defined in the data warehouse.

3. Data loading: Transformed data is loaded into the data warehouse.

### Architecture

The code is organized according to the following architecture:

- `main.py` : Main application code.
- `sql`: Directory containing the sql scripts used to create the data warehouse tables.
- `src` : Directory containing the application's python modules.
    - `orm.py` : Module containing database connection functions.
    - `X_etl.py` : Module containing ETL functions (there is one `etl.py` file for each type of source, e.g. `InD_etl.py`).
    - `logger.py`: Module containing logging functions (WIP).

The data access layer is implemented with SQLAlchemy. For more information on SQLAlchemy, see [https://www.sqlalchemy.org/](https://www.sqlalchemy.org/).

In this way, the data access layer is independent of the type of database used. It is possible to change databases without modifying the application code. Each table in the data warehouse is represented by a Python class. These classes are defined in the `orm.py` file.

Each data source has its own ETL module. For example, the `InD_etl.py` module implements ETL functions for data from the InD dataset.

### Data warehouse model

The data warehouse model is defined in the `sql/create_tables.sql` file.

The data warehouse model is as follows:

![Data warehouse model](./img/TrajectoryDW.png)

The data warehouse model is made up of 7 star-shaped tables:

- `trajectory`: Table of trajectory facts.
- `location`: Table of measurement point location dimensions.
- `trajectory_start_time`: Trajectory start time dimension table.
- `trajectory_end_time`: Trajectory end time dimension table.
- `trajectory_start_date`: Trajectory start date dimension table.
- `trajectory_end_date`: Trajectory end date dimension table.
- `tracked_object`: Dimension table of tracked objects.
- `record`: Record dimension table.
- `weather_condition`: Dimension table for weather conditions (as the data in the InD dataset is not time-stamped, it is not possible to associate weather conditions with trajectories from this dataset).

## Configuration

### Prerequisites

The `.env` configuration file is used to configure the working directory and database parameters.

The `example.env` file contains an example configuration file.

Create a `.env` file from the `example.env` file and configure it as required.

Next, create a directory in which to place the csv files containing the input data. This directory must be configured in the `.env` file.

Access to the PostgreSQL database must be configured by specifying the connection parameters in the `.env` file.

### Creating the virtual environment

Create the project's virtual environment by executing the following command:

```bash
python -m venv venv
```

Activate the virtual environment by executing the following command:

```bash
source venv/bin/activate
```

Finally, before running the program, install the project's dependencies by executing the following command:

```bash
pip install -r requirements.txt
```

### Creating the database

To create the database, connect to the database's Docker container by running the following command:

```bash
docker exec -it TrajectoriesDW_DB psql -U postgres
```

TrajectoriesDW_DB is the name of the database's Docker container. This information is defined in the `docker-compose.yml` file.

Next, create the database by running the following command:

```bash
postgres=# CREATE DATABASE inddataset;
CREATE DATABASE
```

We then create the user `inddataset` and give him rights to the `inddataset` database:

```bash
postgres=# CREATE USER inddataset WITH PASSWORD 'inddataset';
CREATE ROLE
postgres=# GRANT ALL PRIVILEGES ON DATABASE inddataset TO inddataset;
GRANT
postgres=# ALTER DATABASE inddataset OWNER TO inddataset; 
ALTER DATABASE
```

For each table, the user `inddataset` must be given rights:

```bash
postgres=# GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO inddataset;
GRANT
postgres=# GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO inddataset;
GRANT
postgres=# GRANT ALL ON ALL TABLES IN SCHEMA public TO inddataset;
GRANT
postgres=# GRANT ALL ON ALL SEQUENCES IN SCHEMA public TO inddataset;
GRANT
```

If this is not enough for each table, you can add specific rights to the `location` table, for example :
    
```bash
postgres=# GRANT ALL ON TABLE public.location TO inddataset;
GRANT
...
```

Replace `inddataset` with the database name, user and password configured in the `.env` file (`DB_NAME`, `DB_USER` and `DB_PASSWARD`).

The `\l` command is used to list databases:

```bash
postgres=# \l
                                                          List of databases
    Name    |   Owner    | Encoding | Locale Provider |  Collate   |   Ctype    | ICU Locale | ICU Rules |     Access privileges     
------------+------------+----------+-----------------+------------+------------+------------+-----------+---------------------------
 inddataset | inddataset | UTF8     | libc            | en_US.utf8 | en_US.utf8 |            |           | =Tc/inddataset           +
            |            |          |                 |            |            |            |           | inddataset=CTc/inddataset
 postgres   | postgres   | UTF8     | libc            | en_US.utf8 | en_US.utf8 |            |           | 
 template0  | postgres   | UTF8     | libc            | en_US.utf8 | en_US.utf8 |            |           | =c/postgres              +
            |            |          |                 |            |            |            |           | postgres=CTc/postgres
 template1  | postgres   | UTF8     | libc            | en_US.utf8 | en_US.utf8 |            |           | =c/postgres              +
            |            |          |                 |            |            |            |           | postgres=CTc/postgres
(4 rows)
```

Exit the database prompt with the following command:

```bash
postgres=# \q
```

### Creating tables

Before launching the program, you need to create the data warehouse tables by running the `create_tables.sql` script, located in the `sql` directory.

To run the sql script in the container, type the following command:

```bash
docker exec -i TrajectoriesDW_DB psql -U postgres -d inddataset < ./sql/create_tables.sql
```

## Usage

Copy the csv files containing the input data to the directory configured in the `.env` file.
The csv files must respect the InD dataset format. More information on the InD dataset is available at [https://levelxdata.com/ind-dataset/](https://levelxdata.com/ind-dataset/).

To execute the code in `main.py`, you can use the following command:

```bash
python main.py
```

## Unit tests

Unit tests are implemented in the `test_main.py` file.

To run unit tests, you can use the following command:

```bash
python -m unittest test_main.py
```

## Exploiting data from the data warehouse

To exploit data in the data warehouse, you need a SQL query tool. With a PostgreSQL database, you can use the pgAdmin tool. For more information on pgAdmin, see [https://www.pgadmin.org/](https://www.pgadmin.org/).

To exploit the data in the data warehouse, for example, you can run the following SQL query:

```sql
SELECT
    tracked_object. "class" AS class,
    trajectory_start_date.weekday AS day_of_week,
    "location". "speedLimit" AS speed_limit,
    COUNT(Trajectory.id) AS total_trajectories
FROM
    tracked_object 
JOIN
    trajectory Trajectory ON tracked_object.id = Trajectory.tracked_object_id
JOIN
    trajectory_start_date ON Trajectory.trajectory_start_date_id = trajectory_start_date.id
JOIN
    "location" ON Trajectory.location_id = "location".id
GROUP BY
    GROUPING SETS ((tracked_object. "class", trajectory_start_date.weekday, "location". "speedLimit"), 
                   (tracked_object. "class", trajectory_start_date.weekday), 
                   (tracked_object. "class"), 
                   (trajectory_start_date.weekday), 
                   ())
ORDER BY
    tracked_object. "class", trajectory_start_date.weekday, "location". "speedLimit";
```

This query counts the number of trajectories per day of the week, per tracked object class and per speed limit.

For more information on SQL queries, see [https://sql.sh/](https://sql.sh/).

## License

This software and all its source code are licensed under the CeCILL v2.1 license. For more information on the CeCILL v2.1 license, see [https://cecill.info/licences/Licence_CeCILL_V2.1-en.txt](https://cecill.info/licences/Licence_CeCILL_V2.1-en.txt).

## Authors

- Jean-Paul Garrigos <jean-paul.garrigos@cerema.fr>
