# Changelog

Toutes les modifications notables de ce projet seront documentées dans ce fichier.

Le format est basé sur [Keep a Changelog](https://keepachangelog.com/fr/1.0.0/),
et ce projet adhère à [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Non publié]

### Ajouté

- Néant

### Modifié

- Néant

### Supprimé

- Néant

## [1.1.0] - 2024-02-02

### Ajouté

- Ajout des fonctionnalités de calcul des indicateurs agrégés pour les trajectoires

### Modifié

- Modification du fichier create_tables.sql afin de prendre en compte la version 3 du modèle de données
- Mise à jour des fichiers README.md pour prendre en compte la version 3 du modèle de données

### Supprimé

- Néant

## [1.0.2] - 2024-01-17

### Ajouté

- Néant

### Modifié

- Correction du README.md (la section anglaise présentait des erreurs de présentation)

### Supprimé

- Néant

## [1.0.1] - 2024-01-03

### Ajouté

- Ajout d'une section décrivant l'exploitation des données du Data Warehouse dans le README.md

### Modifié

- Correction du README.md (ancre anglais qui ne fonctionnait pas)

### Supprimé

- Néant

## [1.0.0] - 2024-01-03

### Ajouté

- Mise en place de la structure du projet
- Ajout de la fonctionnalité d'extration des données du jeu de données InD 
- Ajout de la fonctionnalité de transformation des données InD vers le format de données compatible avec le modèle du Data Warehouse
- Ajout de la fonctionnalité de chargement des données dans le Data Warehouse
- Ajout de la fonctionnalité de configuration du répertoire de travail et des paramètres de la base de données à partir du fichier de configuration
- Ajout des tests unitaires pour les fonctionnalités d'extraction et de transformation des données
- Ajout d'un docker-compose pour lancer la base de données PostgreSQL
- Ajout du fichier sql/create_tables.sql pour créer le schéma de la base de données
- Ajout d'un ORM basé sur SQLAlchemy pour accéder à la base de données
- Refactoring du shéma pour centrer l'étoile autour de la table des trajectoires
- Finalisation de la fonctionnalité de chargement des données dans le Data Warehouse
- Ajout d'indicateurs de mesure du temps d'exécution des fonctionnalités d'extraction, de transformation et de chargement des données
- Réfactoring du code pour placer les sources dans le répertoire src
- Ajout de la licence CeCILL v2.1

### Modifié

- Néant

### Supprimé

- Néant

