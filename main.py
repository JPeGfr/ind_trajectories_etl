"""
This function extracts the raw data from CSV files for a specific recording in the InD dataset.

Parameters:
- csv_directory (str): The directory path where the CSV files are located.
- recording_id (str): The recording ID.

Returns:
- raw_data_recordings_meta (pandas DataFrame): The raw data for the recordings meta.
- raw_data_tracks_meta (pandas DataFrame): The raw data for the tracks meta.
- raw_data_tracks (pandas DataFrame): The raw data for the tracks.

Note:
- The function assumes that the CSV files are named in the format "{recording_id}_recordingMeta.csv", "{recording_id}_tracksMeta.csv", and "{recording_id}_tracks.csv".
- The function raises a RuntimeError if no CSV files are found in the directory.
"""
# Copyright Cerema (https://www.cerema.fr)
# contributor(s) : 
# Jean-Paul GARRIGOS (3 Janvier 2024) - jean-paul.garrigos@cerema.fr

# This software is a computer program whose purpose is to create a data processing chain that reads input data, processes it and loads it into the data warehouse.
# The input data is conforms with InD dataset data.

# This software is governed by the CeCILL license under French law and abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 

# As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the economic rights,  and the successive licensors  have only  limited
# liability. 

# In this respect, the user's attention is drawn to the risks associated with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software, that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the same conditions as regards security. 

# The fact that you are presently reading this means that you have had knowledge of the CeCILL license and that you accept its terms.

from dotenv import dotenv_values
import psycopg2

from sqlalchemy import create_engine

from src.InD_etl import process_directory

# Calling up the main function
if __name__ == "__main__":
    # Load parameters from .env file
    env = dotenv_values(".env")
    csv_directory = env["CSV_DIRECTORY"]
    # Connection to the PostgreSQL database
    try:
        # Creates the database connection string
        db_url = f"postgresql+psycopg2://{env['DB_USER']}:{env['DB_PASSWORD']}@{env['DB_HOST']}:{env['DB_HOST_PORT']}/{env['DB_NAME']}"
        # Creates the database engine
        engine = create_engine(db_url)
        print("Connected to the PostgreSQL database!")
        print("Processing files...")
        # Calling up the main function
        files_sum, records_sum, trajectories_sum, extraction_time, transformation_time = process_directory(csv_directory, engine)
        # Closing the connection
        engine.connect().close()
        # Displaying the results
        print(f"Processed {files_sum} files.")
        print(f"Created {records_sum} records.")
        print(f"Created {trajectories_sum} trajectories.")
        print(f"Extraction time: {extraction_time} seconds.")
        print(f"Transformation and loading time: {transformation_time} seconds.")
        print("Done!")
    except psycopg2.OperationalError as e:
        print("Error connecting to the database:", e)
        print("Please check the connection parameters in the .env file.")
        print("If you are using Docker, please check the database container is running.")
    except RuntimeError as e:
        print("Error processing files:", e)
        print("Please check the CSV_DIRECTORY parameter in the .env file and there are files in the directory.")