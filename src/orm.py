# Copyright Cerema (https://www.cerema.fr)
# contributor(s) : 
# Jean-Paul GARRIGOS (3 Janvier 2024) - jean-paul.garrigos@cerema.fr

# This software is a computer program whose purpose is to create a data processing chain that reads input data, processes it and loads it into the data warehouse.
# The input data is conforms with InD dataset data.

# This software is governed by the CeCILL license under French law and abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 

# As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the economic rights,  and the successive licensors  have only  limited
# liability. 

# In this respect, the user's attention is drawn to the risks associated with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software, that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the same conditions as regards security. 

# The fact that you are presently reading this means that you have had knowledge of the CeCILL license and that you accept its terms.

from datetime import datetime
from sqlalchemy import Column, Integer, String, Float, DateTime, ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.orm import Mapped
from sqlalchemy.orm import mapped_column

Base = automap_base()
class Location(Base):
    """
    Class representing the 'location' table in the database.

    Attributes:
        - id (int): Primary key for the location.
        - location_id (int): Dataset specific identifier for the location.
        - speed_limit (float): The speed limit associated with the location.
        - latitude (float): Latitude coordinate of the location.
        - longitude (float): Longitude coordinate of the location.
        - imported_at (datetime): The timestamp of when the record was imported.
    """

    __tablename__ = 'location'
    __table_args__ = {'schema': 'public'}

    id = Column("id", Integer, primary_key=True, autoincrement=True)
    location_id = Column("location_id", Integer)
    speed_limit = Column("speedLimit", Float)
    latitude = Column("latitude", Float)
    longitude = Column("longitude", Float)
    imported_at = Column("imported_at", DateTime, default=datetime.utcnow)
    def __repr__(self):
        """
        Return a string representation of the Location.

        Returns:
        str: String representation of the Location.
        """
        return "<Location(id='%s', speed_limit='%s', latitude='%s', longitude='%s', x_utm_origin='%s', y_utm_origin='%s', ortho_px_to_meter='%s', imported_at='%s')>" % (
            self.id, self.speed_limit, self.latitude, self.longitude, self.x_utm_origin, self.y_utm_origin, self.ortho_px_to_meter, self.imported_at)


class Record(Base):
    '''Represents a record in the database. ie a recording of a trajectory.
    
    Attributes:
        - id (int): Primary key identifier.
        - record_id (int): Dataset specific identifier for the recording.
        - frame_rate (int): Frame rate of the recording.
        - duration (float): Duration of the recording.
        - x_utm_origin (float): X UTM origin coordinate.
        - y_utm_origin (float): Y UTM origin coordinate.
        - ortho_px_to_meter (float): Ortho pixel to meter ratio.
        - num_tracks (int): Number of tracks in the recording.
        - num_vehicles (int): Number of vehicles in the recording.
        - num_vrus (int): Number of VRUs in the recording.
        - imported_at (DateTime): Timestamp indicating when the record was imported.

    Methods:
        __repr__: Returns a string representation of the Record instance.

    '''
    __tablename__ = 'record'
    __table_args__ = {'schema': 'public'}

    id = Column("id", Integer, primary_key=True, autoincrement=True)
    record_id = Column("record_id", Integer)
    frame_rate = Column("framerate", Integer)
    duration = Column("duration", Float)
    x_utm_origin = Column("xUtmOrigin", Float)
    y_utm_origin = Column("yUtmOrigin", Float)
    ortho_px_to_meter = Column("orthoPxToMeter", Float)
    num_tracks = Column("num_tracks", Integer)
    num_vehicules = Column("num_vehicules", Integer)
    num_vrus = Column("num_vrus", Integer)
    imported_at = Column("imported_at", DateTime, default=datetime.utcnow)
    def __repr__(self):
        '''Returns a string representation of the Record instance.'''
        return "<Record(id='%s', frame_rate='%s', duration='%s', x_utm_origin='%s', y_utm_origin='%s', ortho_px_to_meter='%s', num_tracks='%s', num_vehicules='%s', num_vrus='%s', imported_at='%s')>" % (
            self.id, self.frame_rate, self.x_utm_origin, self.y_utm_origin, self.ortho_px_to_meter, self.num_tracks, self.num_vehicules, self.num_vrus, self.imported_at)

class TrajectoryStartTime(Base):
    """Represents the start time of a trajectory.

    Attributes:
        - id (int): Primary key identifier.
        - hour (int): Hour of the trajectory start time.
        - minute (int): Minute of the trajectory start time.
        - second (int): Second of the trajectory start time.
        - imported_at (DateTime): Timestamp indicating when the trajectory start time was imported.

    Methods:
        __repr__: Returns a string representation of the TrajectoryStartTime instance.

    Table Configuration:
        __tablename__ (str): The name of the database table ('trajectory_start_time').
        __table_args__ (dict): Additional table arguments.

    Example:
        start_time = TrajectoryStartTime(hour=8, minute=30, second=0)
    """

    __tablename__ = 'trajectory_start_time'
    __table_args__ = {'schema': 'public'}

    id = Column("id", Integer, primary_key=True, autoincrement=True)
    hour = Column("hour", Integer)
    minute = Column("minute", Integer)
    second = Column("second", Integer)
    imported_at = Column("imported_at", DateTime, default=datetime.utcnow)

    def __repr__(self):
        """Returns a string representation of the TrajectoryStartTime instance."""
        return f"<TrajectoryStartTime(id='{self.id}', hour='{self.hour}', minute='{self.minute}', " \
               f"second='{self.second}', imported_at='{self.imported_at}')>"
    
class TrajectoryEndTime(Base):
    """Represents the end time of a trajectory.

    Attributes:
        - id (int): Primary key identifier.
        - hour (int): Hour of the trajectory end time.
        - minute (int): Minute of the trajectory end time.
        - second (int): Second of the trajectory end time.
        - imported_at (DateTime): Timestamp indicating when the trajectory end time was imported.

    Methods:
        __repr__: Returns a string representation of the TrajectoryEndTime instance.

    Table Configuration:
        __tablename__ (str): The name of the database table ('trajectory_end_time').
        __table_args__ (dict): Additional table arguments.

    Example:
        end_time = TrajectoryEndTime(hour=8, minute=30, second=0)
    """

    __tablename__ = 'trajectory_end_time'
    __table_args__ = {'schema': 'public'}

    id = Column("id", Integer, primary_key=True, autoincrement=True)
    hour = Column("hour", Integer)
    minute = Column("minute", Integer)
    second = Column("second", Integer)
    imported_at = Column("imported_at", DateTime, default=datetime.utcnow)

    def __repr__(self):
        """Returns a string representation of the TrajectoryEndTime instance."""
        return f"<TrajectoryEndTime(id='{self.id}', hour='{self.hour}', minute='{self.minute}', " \
               f"second='{self.second}', imported_at='{self.imported_at}')>"

class TrajectoryStartDate(Base):
    """Represents the start date of a trajectory.

    Attributes:
        - id (int): Primary key identifier.
        - weekday (str): Day of the week.
        - day (int): Day of the trajectory start date.
        - week (int): Week of the year for the trajectory start date.
        - month (int): Month of the trajectory start date.
        - year (int): Year of the trajectory start date.
        - imported_at (DateTime): Timestamp indicating when the trajectory start date was imported.

    Methods:
        __repr__: Returns a string representation of the TrajectoryStartDate instance.

    Table Configuration:
        __tablename__ (str): The name of the database table ('trajectory_start_date').
        __table_args__ (dict): Additional table arguments.

    Example:
        start_date = TrajectoryStartDate(weekday='Monday', day=15, week=3, month=5, year=2023)
    """

    __tablename__ = 'trajectory_start_date'
    __table_args__ = {'schema': 'public'}

    id = Column("id", Integer, primary_key=True, autoincrement=True)
    weekday = Column("weekday", String)
    day = Column("day", Integer)
    week = Column("week", Integer)
    month = Column("month", Integer)
    year = Column("year", Integer)
    imported_at = Column("imported_at", DateTime, default=datetime.utcnow)

    def __repr__(self):
        """Returns a string representation of the TrajectoryStartDate instance."""
        return f"<TrajectoryStartDate(id='{self.id}', weekday='{self.weekday}', day='{self.day}', " \
               f"week='{self.week}', month='{self.month}', year='{self.year}', imported_at='{self.imported_at}')>"

class TrajectoryEndDate(Base):
    """Represents the end date of a trajectory.

    Attributes:
        - id (int): Primary key identifier.
        - weekday (str): Day of the week.
        - day (int): Day of the trajectory end date.
        - week (int): Week of the year for the trajectory end date.
        - month (int): Month of the trajectory end date.
        - year (int): Year of the trajectory end date.
        - imported_at (DateTime): Timestamp indicating when the trajectory end date was imported.

    Methods:
        __repr__: Returns a string representation of the TrajectoryEndDate instance.

    Table Configuration:
        __tablename__ (str): The name of the database table ('trajectory_end_date').
        __table_args__ (dict): Additional table arguments.

    Example:
        end_date = TrajectoryEndDate(weekday='Monday', day=15, week=3, month=5, year=2023)
    """

    __tablename__ = 'trajectory_end_date'
    __table_args__ = {'schema': 'public'}

    id = Column("id", Integer, primary_key=True, autoincrement=True)
    weekday = Column("weekday", String)
    day = Column("day", Integer)
    week = Column("week", Integer)
    month = Column("month", Integer)
    year = Column("year", Integer)
    imported_at = Column("imported_at", DateTime, default=datetime.utcnow)

    def __repr__(self):
        """Returns a string representation of the TrajectoryEndDate instance."""
        return f"<TrajectoryEndDate(id='{self.id}', weekday='{self.weekday}', day='{self.day}', " \
               f"week='{self.week}', month='{self.month}', year='{self.year}', imported_at='{self.imported_at}')>"

class TrackedObject(Base):
    """Represents a tracked object.

    Attributes:
        - id (int): Primary key identifier.
        - width (float): Width of the object.
        - length (float): Length of the object.
        - object_class (str): Class of the object.
        - imported_at (DateTime): Timestamp indicating when the object was imported.

    Methods:
        __repr__: Returns a string representation of the TrackedObject instance.

    Table Configuration:
        __tablename__ (str): The name of the database table ('tracked_object').
        __table_args__ (dict): Additional table arguments.

    Example:
        car = TrackedObject(width=2.0, length=4.5, object_class='Car')
    """

    __tablename__ = 'tracked_object'
    __table_args__ = {'schema': 'public'}

    id = mapped_column("id", Integer, primary_key=True, autoincrement=True)
    width = mapped_column("width", Float)
    length = mapped_column("length", Float)
    object_class = mapped_column("class", String(255))
    imported_at = mapped_column("imported_at", DateTime, default=datetime.utcnow)

    def __repr__(self):
        """Returns a string representation of the TrackedObject instance."""
        return f"<TrackedObject(id='{self.id}', width='{self.width}', length='{self.length}', " \
               f"object_class='{self.object_class}', imported_at='{self.imported_at}')>"

class WeatherCondition(Base):
    """Represents weather conditions.

    Attributes:
        - id (int): Primary key identifier.
        - city_name (str): Name of the city.
        - lat (float): Latitude.
        - long (float): Longitude.
        - dt (int): Date and time in Unix timestamp format.
        - sunrise (int): Sunrise time in Unix timestamp format.
        - sunset (int): Sunset time in Unix timestamp format.
        - temp (float): Temperature.
        - feels_like (float): "Feels like" temperature.
        - pressure (int): Atmospheric pressure.
        - humidity (int): Humidity percentage.
        - dew_point (float): Dew point temperature.
        - uvi (float): UV index.
        - clouds (int): Cloudiness percentage.
        - visibility (int): Visibility distance.
        - wind_speed (float): Wind speed.
        - wind_deg (float): Wind direction in degrees.
        - weather_id (int): Weather condition identifier.
        - weather_main (str): Main weather category.
        - weather_description (str): Description of the weather condition.
        - weather_icon (str): Weather icon identifier.
        - imported_at (DateTime): Timestamp indicating when the weather condition was imported.

    Methods:
        __repr__: Returns a string representation of the WeatherCondition instance.

    Table Configuration:
        __tablename__ (str): The name of the database table ('weather_condition').
        __table_args__ (dict): Additional table arguments.

    Example:
        current_weather = WeatherCondition(city_name='Paris', lat=48.8566, long=2.3522, temp=25.0, humidity=50)
    """

    __tablename__ = 'weather_condition'
    __table_args__ = {'schema': 'public'}

    id = Column("id", Integer, primary_key=True, autoincrement=True)
    city_name = Column("city_name", String(255))
    lat = Column("lat", Float)
    long = Column("long", Float)
    dt = Column("dt", Integer)
    sunrise = Column("sunrise", Integer)
    sunset = Column("sunset", Integer)
    temp = Column("temp", Float)
    feels_like = Column("feels_like", Float)
    pressure = Column("pressure", Integer)
    humidity = Column("humidity", Integer)
    dew_point = Column("dew_point", Float)
    uvi = Column("uvi", Float)
    clouds = Column("clouds", Integer)
    visibility = Column("visibility", Integer)
    wind_speed = Column("wind_speed", Float)
    wind_deg = Column("wind_deg", Float)
    weather_id = Column("weather_id", Integer)
    weather_main = Column("weather_main", String(255))
    weather_description = Column("weather_description", String(255))
    weather_icon = Column("weather_icon", String(255))
    imported_at = Column("imported_at", DateTime, default=datetime.utcnow)

    def __repr__(self):
        """Returns a string representation of the WeatherCondition instance."""
        return f"<WeatherCondition(id='{self.id}', city_name='{self.city_name}', lat='{self.lat}', long='{self.long}', " \
               f"temp='{self.temp}', humidity='{self.humidity}', imported_at='{self.imported_at}')>"

class Trajectory(Base):
    """Represents a trajectory.

    Attributes:
        - id (int): Primary key identifier.
        - tracked_object_id (int): Foreign key referencing the TrackedObject associated with the trajectory.
        - location_id (int): Foreign key referencing the Location associated with the trajectory.
        - trajectory_start_time_id (int): Foreign key referencing the TrajectoryStartTime associated with the trajectory.
        - trajectory_start_date_id (int): Foreign key referencing the TrajectoryStartDate associated with the trajectory.
        - weather_condition_id (int): Foreign key referencing the WeatherCondition associated with the trajectory.
        - record_id (int): Foreign key referencing the Record associated with the trajectory.
        - initial_frame (int): Initial frame of the trajectory.
        - final_frame (int): Final frame of the trajectory.
        - num_frames (int): Number of frames in the trajectory.
        - duration (int): Duration of the trajectory.
        - distance_travelled (float): Distance travelled by the trajectory.
        - average_velocity (float): Average velocity of the trajectory.
        - max_velocity (float): Maximum velocity of the trajectory.
        - min_velocity (float): Minimum velocity of the trajectory.
        - imported_at (DateTime): Timestamp indicating when the trajectory was imported.

    Relationships:
        object (TrackedObject): Relationship with the TrackedObject class.
        location (Location): Relationship with the Location class.
        trajectory_start_time (TrajectoryStartTime): Relationship with the TrajectoryStartTime class.
        trajectory_start_date (TrajectoryStartDate): Relationship with the TrajectoryStartDate class.
        weather_condition (WeatherCondition): Relationship with the WeatherCondition class.
        record (Record): Relationship with the Record class.

    Methods:
        __repr__: Returns a string representation of the Trajectory instance.

    Table Configuration:
        __tablename__ (str): The name of the database table ('trajectory').

    Example:
        traj = Trajectory(object_id=1, location_id=2, trajectory_start_time_id=3, trajectory_start_date_id=4,
                          weather_condition_id=5, record_id=6, initial_frame=7, final_frame=10, num_frames=300,
                          duration=60, imported_at=datetime.utcnow())
    """

    __tablename__ = 'trajectory'
    __table_args__ = {'schema': 'public'}

    id = mapped_column("id", Integer, primary_key=True, autoincrement=True)

    # tracked_object_id = mapped_column("tracked_object_id", Integer, ForeignKey('tracked_object.id'))
    tracked_object_id = mapped_column("tracked_object_id", Integer)
    location_id = mapped_column("location_id", Integer)
    trajectory_start_time_id = mapped_column("trajectory_start_time_id", Integer)
    trajectory_start_date_id = mapped_column("trajectory_start_date_id", Integer)
    weather_condition_id = mapped_column("weather_condition_id", Integer)
    record_id = mapped_column("record_id", Integer)
    # tracked_object = relationship('TrackedObject', primaryjoin='tracked_object.id == trajectory.tracked_object_id')
    # location_id = Column("location_id", Integer, ForeignKey('location.id'))
    # trajectory_start_time_id = Column("trajectory_start_time_id", Integer, ForeignKey('trajectory_start_time.id'))
    # trajectory_start_date_id = Column("trajectory_start_date_id", Integer, ForeignKey('trajectory_start_date.id'))
    # weather_condition_id = Column("weather_condition_id", Integer, ForeignKey('weather_condition.id'))
    # record_id = Column("record_id", Integer, ForeignKey('record.id'))
    trajectory_id = mapped_column("trajectory_id", Integer)
    initial_frame = mapped_column("initial_frame", Integer)
    final_frame = mapped_column("final_frame", Integer)
    num_frames = mapped_column("num_frames", Integer)
    duration = mapped_column("duration", Float)
    distance_travelled = mapped_column("distance_travelled", Float)
    average_velocity = mapped_column("average_velocity", Float)
    max_velocity = mapped_column("max_velocity", Float)
    min_velocity = mapped_column("min_velocity", Float)
    imported_at = mapped_column("imported_at", DateTime, default=datetime.utcnow)

    # Relationships
    # location = relationship('Location', backref='trajectories')
    # trajectory_start_time = relationship('TrajectoryStartTime', backref='trajectories')
    # trajectory_start_date = relationship('TrajectoryStartDate', backref='trajectories')
    # weather_condition = relationship('WeatherCondition', backref='trajectories')
    # record = relationship('Record', backref='trajectories')

    # def __repr__(self):
    #     """Returns a string representation of the Trajectory instance."""
    #     return f"<Trajectory(id='{self.id}', object_id='{self.object_id}', location_id='{self.location_id}', " \
    #            f"trajectory_start_time_id='{self.trajectory_start_time_id}', " \
    #            f"trajectory_start_date_id='{self.trajectory_start_date_id}', " \
    #            f"weather_condition_id='{self.weather_condition_id}', record_id='{self.record_id}', " \
    #            f"initial_frame='{self.initial_frame}', final_frame='{self.final_frame}', " \
    #            f"num_frames='{self.num_frames}', duration='{self.duration}', " \
    #            f"imported_at='{self.imported_at}')>"