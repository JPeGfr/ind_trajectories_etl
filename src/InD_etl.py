"""
This code snippet is a part of a data processing chain that reads input data, processes it, and loads it into a data warehouse. It is specifically designed to work with the InD dataset.

The code includes two functions:
1. InD_extract_data(csv_directory, recording_id): This function extracts the raw data from CSV files for a specific recording in the InD dataset. It takes the directory path where the CSV files are located and the recording ID as inputs and returns the raw data for the recordings meta, tracks meta, and tracks.

2. InD_transform_n_load_data(engine, raw_data_recordings_meta, raw_data_tracks_meta, raw_data_tracks): This function transforms the raw data extracted from the InD dataset and loads it into a database using SQLAlchemy. It takes the database engine and the raw data for recordings meta, tracks meta, and tracks as inputs. The function performs various data manipulations, creates objects representing different entities (Location, Record, TrajectoryStartTime, etc.), and adds them to the session for committing to the database.

Additionally, there is a process_directory(directory, engine) function that processes all the files in a directory. It takes the directory path and the database engine as inputs and returns the number of files, records, and trajectories created.

The code also includes some utility functions and imports necessary libraries.

Note: The code includes comments explaining the purpose and functionality of each section.
"""
# Copyright Cerema (https://www.cerema.fr)
# contributor(s) : 
# Jean-Paul GARRIGOS (3 Janvier 2024) - jean-paul.garrigos@cerema.fr

# This software is a computer program whose purpose is to create a data processing chain that reads input data, processes it and loads it into the data warehouse.
# The input data is conforms with InD dataset data.

# This software is governed by the CeCILL license under French law and abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 

# As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the economic rights,  and the successive licensors  have only  limited
# liability. 

# In this respect, the user's attention is drawn to the risks associated with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software, that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the same conditions as regards security. 

# The fact that you are presently reading this means that you have had knowledge of the CeCILL license and that you accept its terms.

import math
import os
import warnings
from pandas.errors import SettingWithCopyWarning
import numpy as np
import pandas as pd
from sqlalchemy.orm import sessionmaker

from src.orm import *

from datetime import datetime

def InD_extract_data(csv_directory, recording_id):
    '''
    This function extracts the InD dataset raw data from CSV files.
    It returns the raw data for the recordings meta, tracks meta and tracks.
    '''
    # Load Meta record data from CSV file
    recordings_meta = pd.read_csv(f"{csv_directory}/{recording_id}_recordingMeta.csv")
    # Load Meta track data from CSV file
    tracks_meta = pd.read_csv(f"{csv_directory}/{recording_id}_tracksMeta.csv")
    # Load track data from CSV file
    tracks = pd.read_csv(f"{csv_directory}/{recording_id}_tracks.csv")
    return recordings_meta, tracks_meta, tracks

def InD_transform_n_load_data(engine, raw_data_recordings_meta, raw_data_tracks_meta, raw_data_tracks):
    '''
    This function transforms the InD dataset raw data and loads it into the database.
    '''

    # Bind the engine to the metadata of the Base class so that the
    # declaratives can be accessed through a DBSession instance
    Base.prepare(autoload_with=engine)
    # Create a session
    Session = sessionmaker(bind=engine)
    session = Session()

    # Rename the colomn "width" to "tracked_object_width"
    raw_data_tracks_meta.rename(columns={'width': 'tracked_object_width', 'length': 'tracked_object_length'}, inplace=True)
    # Rename the colomn "width" to "object_width"
    raw_data_tracks.rename(columns={'width': 'object_width', 'length': 'object_length'}, inplace=True)
    # Merge data to obtain complete record data set
    all_tracks_merged_data = pd.merge(raw_data_tracks_meta, raw_data_recordings_meta, on='recordingId')
    all_track_points_merged_data = pd.merge(raw_data_tracks, all_tracks_merged_data, on=['recordingId', 'trackId'])

    # Iterate over the rows of the tracks dataframe merged with the records dataframe
    for _, row in all_tracks_merged_data.iterrows():

        # Create Location objects from raw data
        # - id (int): Primary key for the location.
        # - speed_limit (float): The speed limit associated with the location.
        # - latitude (float): Latitude coordinate of the location.
        # - longitude (float): Longitude coordinate of the location.
        # - imported_at (datetime): The timestamp of when the record was imported.
        location = Location(
            location_id = row['locationId'],
            speed_limit = row['speedLimit'],
            latitude = row['latLocation'],
            longitude = row['lonLocation'],
            imported_at = datetime.now()
            )
        session.add(location)
        # Commit the changes    
        session.commit()

        # Create Record objects from raw data
        # - id (int): Primary key identifier.
        # - frame_rate (int): Frame rate of the recording.
        # - duration (int): Duration of the recording.
        # - x_utm_origin (float): X UTM origin coordinate.
        # - y_utm_origin (float): Y UTM origin coordinate.
        # - ortho_px_to_meter (float): Ortho pixel to meter ratio.
        # - num_tracks (int): Number of tracks in the recording.
        # - num_vehicules (int): Number of vehicles in the recording.
        # - num_vrus (int): Number of VRUs in the recording.
        # - imported_at (DateTime): Timestamp indicating when the record was imported.
        record = Record(
            record_id = row['recordingId'],
            frame_rate = row['frameRate'],
            duration = row['duration'],
            x_utm_origin = row['xUtmOrigin'],
            y_utm_origin = row['yUtmOrigin'],
            ortho_px_to_meter = row['orthoPxToMeter'],
            num_tracks = row['numTracks'],
            num_vehicules = row['numVehicles'],
            num_vrus = row['numVRUs'],
            imported_at = datetime.now()
            )
        session.add(record)
        # Commit the changes
        session.commit()

        # Create TrajectoryStartTime objects from raw data
        # - id (int): Primary key identifier.
        # - hour (int): Hour of the trajectory start time.
        # - minute (int): Minute of the trajectory start time.
        # - second (int): Second of the trajectory start time.
        # - imported_at (DateTime): Timestamp indicating when the trajectory start time was imported.
        trajectory_start_time = TrajectoryStartTime(
            
            # Checks that the value is not NaN before assigning it to the variable
            hour = int(row['startTime']) if not math.isnan(row['startTime']) else None,
            # In InD dataset, the minute and second are always None
            imported_at = datetime.now()
            )
        session.add(trajectory_start_time)
        # Commit the changes
        session.commit()

        # Create TrajectoryStartDate objects from raw data
        # - id (int): Primary key identifier.
        # - weekday (str): Day of the week.
        # - day (int): Day of the trajectory start date.
        # - week (int): Week of the year for the trajectory start date.
        # - month (int): Month of the trajectory start date.
        # - year (int): Year of the trajectory start date.
        # - imported_at (DateTime): Timestamp indicating when the trajectory start date was imported.
        trajectory_start_date = TrajectoryStartDate(
            weekday = row['weekday'],
             # In InD dataset, the day, week, month and year are always None
            imported_at = datetime.now()
            )
        session.add(trajectory_start_date)
        # Commit the changes
        session.commit()

        # Create TrackedObject objects from raw data
        # - id (int): Primary key identifier.
        # - width (float): Width of the object.
        # - length (float): Length of the object.
        # - object_class (str): Class of the object.
        # - imported_at (DateTime): Timestamp indicating when the object was imported.
        tracked_object = TrackedObject(
            width = row['tracked_object_width'],
            length = row['tracked_object_length'],
            object_class = row['class'],
            imported_at = datetime.now()
            )
        session.add(tracked_object)
        # Commit the changes
        session.commit()

        # Select the track points associated with the current track
        track_points = all_track_points_merged_data[all_track_points_merged_data['trackId'] == row['trackId']]

        # Calculate the trajectory aggregate data from the track points
        calculated_num_frames = track_points['frame'].max() - track_points['frame'].min() + 1
        if calculated_num_frames != row['numFrames']:
            # TODO: log the warning
            print(f"Track ID: {row['trackId']}")
            print(f"Warning: the number of frames calculated from the track points ({calculated_num_frames}) is different from the number of frames in the track meta ({row['numFrames']}).")
        calculated_duration = calculated_num_frames / track_points['frameRate'].mean()

        # Calculating the Euclidean distance between successive points
        warnings.filterwarnings("ignore", category=SettingWithCopyWarning) # Ignore the SettingWithCopyWarning
        track_points.loc[:, 'yPrev'] = track_points['yCenter'].shift(1)
        track_points.loc[:, 'xPrev'] = track_points['xCenter'].shift(1)
        # Create a new column for the Euclidean distance
        track_points['euclidean_distance'] = np.nan
        # Filter rows without NaN in 'xPrev' and 'yPrev'.
        valid_rows = track_points.dropna(subset=['xPrev', 'yPrev'])
        # Calculate the Euclidean distance
        track_points.loc[valid_rows.index, 'euclidean_distance'] = valid_rows.apply(lambda row: math.sqrt((row['xCenter'] - row['xPrev'])**2 + (row['yCenter'] - row['yPrev'])**2), axis=1)
        # Calculating the distance travelled
        distance_travelled = track_points['euclidean_distance'].sum()
        # Calculating the average speed
        average_velocity = distance_travelled / calculated_duration
        # Calculating the maximum speed
        max_velocity = track_points['lonVelocity'].max()
        # Calculating the minimum speed
        min_velocity = track_points['lonVelocity'].min()
        
        # Create TrajectoryEndTime objects from raw data
        # - id (int): Primary key identifier.
        # - hour (int): Hour of the trajectory end time.
        # - minute (int): Minute of the trajectory end time.
        # - second (int): Second of the trajectory end time.
        # - imported_at (DateTime): Timestamp indicating when the trajectory end time was imported.
        # In InD dataset, the hour, minute and second are always None

        # Create TrajectoryEndDate objects from raw data
        # - id (int): Primary key identifier.
        # - weekday (str): Day of the week.
        # - day (int): Day of the trajectory end date.
        # - week (int): Week of the year for the trajectory end date.
        # - month (int): Month of the trajectory end date.
        # - year (int): Year of the trajectory end date.
        # - imported_at (DateTime): Timestamp indicating when the trajectory end date was imported.
        # In InD dataset, the weekday, day, week, month and year are always None

        # Create Trajectory objects from raw data
        # - id (int): Primary key identifier.
        # - tracked_object_id (int): Foreign key referencing the TrackedObject associated with the trajectory.
        # - location_id (int): Foreign key referencing the Location associated with the trajectory.
        # - trajectory_start_time_id (int): Foreign key referencing the TrajectoryStartTime associated with the trajectory.
        # - trajectory_start_date_id (int): Foreign key referencing the TrajectoryStartDate associated with the trajectory.
        # - weather_condition_id (int): Foreign key referencing the WeatherCondition associated with the trajectory.
        # - record_id (int): Foreign key referencing the Record associated with the trajectory.
        # - initial_frame (int): Initial frame of the trajectory.
        # - final_frame (int): Final frame of the trajectory.
        # - num_frames (int): Number of frames in the trajectory.
        # - duration (int): Duration of the trajectory.
        # - distance_travelled (float): Distance travelled by the trajectory.
        # - average_velocity (float): Average velocity of the trajectory.
        # - max_velocity (float): Maximum velocity of the trajectory.
        # - min_velocity (float): Minimum velocity of the trajectory.
        # - imported_at (DateTime): Timestamp indicating when the trajectory was imported.
        trajectory = Trajectory(
            tracked_object_id = tracked_object.id,
            location_id = location.id,
            trajectory_start_time_id = trajectory_start_time.id,
            trajectory_start_date_id = trajectory_start_date.id,
            record_id = record.id,
            trajectory_id = row['trackId'],
            initial_frame = row['initialFrame'],
            final_frame = row['finalFrame'],
            num_frames = row['numFrames'],
            duration = calculated_duration,
            distance_travelled = distance_travelled,
            average_velocity = average_velocity,
            max_velocity = max_velocity,
            min_velocity = min_velocity,
            imported_at = datetime.now()
            )
        session.add(trajectory)
        # Commit the changes  
        session.commit()         

def process_directory(directory, engine):
    '''
    This function processes all the files in a directory.
    It returns the number of files, records and trajectories created.
    '''
    
    # Backup begin time
    begin_time = datetime.now()

    # Initialize the number of files, records and trajectories
    files_number = 0
    records_sum = 0
    trajectories_sum = 0
    
    # List all files in the directory
    all_files = os.listdir(directory)

    # Filter relevant CSV files
    csv_files = [file for file in all_files if file.endswith('.csv')]
    files_number = len(csv_files)
    if files_number == 0:
        raise RuntimeError("No CSV files found in the directory.")

    # Extraction, transformation and loading process for each CSV file
    for csv_file in csv_files:
        if csv_file.endswith('_recordingMeta.csv'):
            recording_id = csv_file.split('_')[0]  # Extract the recording identifier
            #csv_directory = os.path.join(directory, recording_id)

            # Extraction
            raw_data_recordings_meta, raw_data_tracks_meta, raw_data_tracks = InD_extract_data(directory, recording_id)

            # Retrieving the number of records
            records_sum = records_sum + raw_data_recordings_meta.shape[0]

            # Retrieving the number of trajectories
            trajectories_sum = trajectories_sum + raw_data_tracks_meta.shape[0]

            # Measurement of the time taken for the extraction
            extraction_time = datetime.now() - begin_time
            # Formatting in seconds
            extraction_time = extraction_time.total_seconds()

            # Transformation and loading
            InD_transform_n_load_data(
                engine,
                raw_data_recordings_meta, raw_data_tracks_meta, raw_data_tracks
            )

            # Measurement of the time taken for the transformation and loading
            transformation_time = datetime.now() - begin_time
            # Formatting in seconds
            transformation_time = transformation_time.total_seconds()

    return files_number, records_sum , trajectories_sum, extraction_time, transformation_time
