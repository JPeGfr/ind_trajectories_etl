# Loading data into the "Location" dimension table
def load_location_table(conn, locations_data):
    cursor = conn.cursor()

    # Create the "Location" table if it doesn't already exist
    cursor.execute("""
        CREATE TABLE IF NOT EXISTS Location (
            locationId INT PRIMARY KEY,
            latLocation REAL,
            lonLocation REAL,
            xUtmOrigin REAL,
            yUtmOrigin REAL,
            orthoPxToMeter REAL
        )
    """)

    # Load data into the "Location" table
    for location in locations_data:
        cursor.execute("""
            INSERT INTO Location
            VALUES (%s, %s, %s, %s, %s, %s)
        """, (
            location.location_id, location.lat_location, location.lon_location,
            location.x_utm_origin, location.y_utm_origin, location.ortho_px_to_meter
        ))

    conn.commit()
    cursor.close()

# Loading data into the "Time" dimension table
def load_time_table(conn, times_data):
    cursor = conn.cursor()

    # Create the "Time" table if it doesn't already exist
    cursor.execute("""
        CREATE TABLE IF NOT EXISTS Time (
            weekday VARCHAR(255) PRIMARY KEY,
            startTime INT,
            duration INT
        )
    """)

    # Load data into the "Time" table
    for time in times_data:
        cursor.execute("""
            INSERT INTO Time
            VALUES (%s, %s, %s)
        """, (
            time.weekday, time.start_time, time.duration
        ))

    conn.commit()
    cursor.close()

# Loading data into the "ObjectClass" dimension table
def load_object_class_table(conn, object_classes_data):
    cursor = conn.cursor()

    # Create the "ObjectClass" table if it doesn't already exist
    cursor.execute("""
        CREATE TABLE IF NOT EXISTS ObjectClass (
            class VARCHAR(255) PRIMARY KEY,
            className VARCHAR(255)
        )
    """)

    # Load data into the "ObjectClass" table
    for object_class in object_classes_data:
        cursor.execute("""
            INSERT INTO ObjectClass
            VALUES (%s, %s)
        """, (
            object_class.class_id, object_class.class_name
        ))

    conn.commit()
    cursor.close()

# Loading data into the "Record" dimension table
def load_record_table(conn, records_data):
    cursor = conn.cursor()

    # Create the "Record" table if it does not already exist
    cursor.execute("""
        CREATE TABLE IF NOT EXISTS Record (
            recordingId INT PRIMARY KEY,
            locationId INT,
            frameRate REAL,
            speedLimit REAL,
            numTracks INT,
            numVehicles INT,
            numVRUs INT
        )
    """)

    # Load data into the "Record" table
    for record in records_data:
        cursor.execute("""
            INSERT INTO Record
            VALUES (%s, %s, %s, %s, %s, %s, %s)
        """, (
            record.recording_id, record.location_id, record.frame_rate, record.speed_limit,
            record.num_tracks, record.num_vehicles, record.num_vrus
        ))

    conn.commit()
    cursor.close()

# Loading data into the "TrackMeta" dimension table
def load_track_meta_table(conn, tracks_meta_data):
    cursor = conn.cursor()

    # Create the "TrackMeta" table if it doesn't already exist
    cursor.execute("""
        CREATE TABLE IF NOT EXISTS TrackMeta (
            recordingId INT,
            trackId INT PRIMARY KEY,
            initialFrame INT,
            finalFrame INT,
            numFrames INT,
            width REAL,
            length REAL,
            class INT
        )
    """)

    # Load data into the "TrackMeta" table
    for track_meta in tracks_meta_data:
        cursor.execute("""
            INSERT INTO TrackMeta
            VALUES (%s, %s, %s, %s, %s, %s, %s, %s)
        """, (
            track_meta.recording_id, track_meta.track_id, track_meta.initial_frame, track_meta.final_frame,
            track_meta.num_frames, track_meta.width, track_meta.length, track_meta.class_id
        ))

    conn.commit()
    cursor.close()

# Loading data into the "Trajectory_points" fact table
def load_trajectories_table(conn, trajectory_points_data):
    cursor = conn.cursor()

    # Create the "Trajectory_points" table if it does not already exist
    cursor.execute("""
        CREATE TABLE IF NOT EXISTS Trajectory_points (
            recordingId INT,
            trackId INT,
            frame INT,
            trackLifetime INT,
            xCenter REAL,
            yCenter REAL,
            heading REAL,
            width REAL,
            length REAL,
            xVelocity REAL,
            yVelocity REAL,
            xAcceleration REAL,
            yAcceleration REAL,
            lonVelocity REAL,
            latVelocity REAL,
            lonAcceleration REAL,
            latAcceleration REAL,
            PRIMARY KEY (recordingId, trackId, frame)
        )
    """)

    # Load data into the "Trajectory_points" table
    for trajectory in trajectory_points_data:
        cursor.execute("""
            INSERT INTO Trajectory_points
            VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
        """, (
            trajectory.recording_id, trajectory.track_id, trajectory.frame, trajectory.track_lifetime,
            trajectory.x_center, trajectory.y_center, trajectory.heading, trajectory.width,
            trajectory.length, trajectory.x_velocity, trajectory.y_velocity,
            trajectory.x_acceleration, trajectory.y_acceleration,
            trajectory.lon_velocity, trajectory.lat_velocity,
            trajectory.lon_acceleration, trajectory.lat_acceleration
        ))

    conn.commit()
    cursor.close()