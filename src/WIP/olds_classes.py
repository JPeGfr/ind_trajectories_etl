"""Definition of the Location class to represent each record in the "Location" dimension table

    :return: A instance of the Location class
    :rtype: Location object
"""
class Location:
    def __init__(self, location_id, lat_location, lon_location, x_utm_origin, y_utm_origin, ortho_px_to_meter):
        self.location_id = location_id
        self.lat_location = lat_location
        self.lon_location = lon_location
        self.x_utm_origin = x_utm_origin
        self.y_utm_origin = y_utm_origin
        self.ortho_px_to_meter = ortho_px_to_meter

# Definition of the Time class to represent each record in the "Time" dimension table
class Time:
    def __init__(self, weekday, start_time, duration):
        self.weekday = weekday
        self.start_time = start_time
        self.duration = duration

# Definition of the ObjectClass class to represent each record in the "ObjectClass" dimension table
class ObjectClass:
    def __init__(self, class_id, class_name):
        self.class_id = class_id
        self.class_name = class_name

# Definition of the Record class to represent each record in the "Record" dimension table
class Record:
    def __init__(self, recording_id, location_id, frame_rate, speed_limit, num_tracks, num_vehicles, num_vrus):
        self.recording_id = recording_id
        self.location_id = location_id
        self.frame_rate = frame_rate
        self.speed_limit = speed_limit
        self.num_tracks = num_tracks
        self.num_vehicles = num_vehicles
        self.num_vrus = num_vrus

# Definition of the TrackMeta class to represent each piece of track information
class TrackMeta:
    def __init__(self, recording_id, track_id, initial_frame, final_frame, num_frames, object_width, object_length, class_id):
        self.recording_id = recording_id
        self.track_id = track_id
        self.initial_frame = initial_frame
        self.final_frame = final_frame
        self.num_frames = num_frames
        self.object_width = object_width
        self.object_length = object_length
        self.class_id = class_id

# Definition of the Trajectory class to represent each trajectory
class Trajectory_point:
    def __init__(self, recording_id, track_id, frame, track_lifetime, x_center, y_center,
                 heading, object_width, object_length, x_velocity, y_velocity,
                 x_acceleration, y_acceleration, lon_velocity, lat_velocity,
                 lon_acceleration, lat_acceleration):
        self.recording_id = recording_id
        self.track_id = track_id
        self.frame = frame
        self.track_lifetime = track_lifetime
        self.x_center = x_center
        self.y_center = y_center
        self.heading = heading
        self.object_width = object_width
        self.object_length = object_length
        self.x_velocity = x_velocity
        self.y_velocity = y_velocity
        self.x_acceleration = x_acceleration
        self.y_acceleration = y_acceleration
        self.lon_velocity = lon_velocity
        self.lat_velocity = lat_velocity
        self.lon_acceleration = lon_acceleration
        self.lat_acceleration = lat_acceleration
