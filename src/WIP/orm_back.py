class Times(Base):
    __tablename__ = 'times'
    __slots__ = ('id', 'hour', 'minute', 'seconde', 'imported_at')

    id = Column(Integer, primary_key=True)
    hour = Column(Integer)
    minute = Column(Integer)
    seconde = Column(Integer)
    imported_at = Column(DateTime, default=datetime.utcnow)

class Dates(Base):
    __tablename__ = 'dates'
    __slots__ = ('id', 'weekday', 'day', 'week', 'month', 'year', 'imported_at')

    id = Column(Integer, primary_key=True)
    weekday = Column(String)
    day = Column(Integer)
    week = Column(Integer)
    month = Column(Integer)
    year = Column(Integer)
    imported_at = Column(DateTime, default=datetime.utcnow)

class Objects(Base):
    __tablename__ = 'objects'
    __slots__ = ('id', 'class_', 'imported_at')

    id = Column(Integer, primary_key=True)
    class_ = Column(String)
    imported_at = Column(DateTime, default=datetime.utcnow)

class TrajectoryPoints(Base):
    __tablename__ = 'trajectory_points'
    __slots__ = ('id', 'frame', 'track_lifetime', 'x_center', 'y_center', 'heading', 'width', 'length', 'x_velocity', 'y_velocity',
                 'lat_velocity', 'lon_velocity', 'lat_acceleration', 'lon_acceleration', 'imported_at')

    id = Column(Integer, primary_key=True)
    frame = Column(Integer)
    track_lifetime = Column(Integer)
    x_center = Column(Float)
    y_center = Column(Float)
    heading = Column(Float)
    width = Column(Float)
    length = Column(Float)
    x_velocity = Column(Float)
    y_velocity = Column(Float)
    lat_velocity = Column(Float)
    lon_velocity = Column(Float)
    lat_acceleration = Column(Float)
    lon_acceleration = Column(Float)
    imported_at = Column(DateTime, default=datetime.utcnow)

class Interactions(Base):
    __tablename__ = 'interactions'
    __slots__ = ('id', 'kind', 'imported_at')

    id = Column(Integer, primary_key=True)
    kind = Column(String)
    imported_at = Column(DateTime, default=datetime.utcnow)