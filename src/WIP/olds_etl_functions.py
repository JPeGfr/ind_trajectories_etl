# InD dataset Data transformation
def InD_transform_data(raw_data_recordings_meta, raw_data_tracks_meta, raw_data_tracks):
    # Rename the colomn "width" to "tracked_object_width"
    raw_data_tracks_meta.rename(columns={'width': 'tracked_object_width', 'length': 'tracked_object_length'}, inplace=True)
    # Rename the colomn "width" to "object_width"
    raw_data_tracks.rename(columns={'width': 'object_width', 'length': 'object_length'}, inplace=True)
    # Merge data to obtain complete information
    merged_data = pd.merge(raw_data_tracks, raw_data_tracks_meta, on=['recordingId', 'trackId'])
    merged_data = pd.merge(merged_data, raw_data_recordings_meta, on='recordingId')

    # Create a list of Location objects from raw data
    locations_list = []
    for _, row in raw_data_recordings_meta.iterrows():
        location = Location(
            row['locationId'], row['latLocation'], row['lonLocation'],
            row['xUtmOrigin'], row['yUtmOrigin'], row['orthoPxToMeter']
        )
        locations_list.append(location)

    # Create a list of Time objects from raw data
    times_list = []
    for _, row in raw_data_recordings_meta.iterrows():
        time = Time(row['weekday'], row['startTime'], row['duration'])
        times_list.append(time)

    # Create a list of ObjectClass objects from raw data
    object_classes_list = []
    for _, row in raw_data_tracks_meta.iterrows():
        object_class = ObjectClass(row['class'], f"Class_{row['class']}")
        object_classes_list.append(object_class)

    # Create a list of Record objects from raw data
    records_list = []
    for _, row in merged_data.iterrows():
        record = Record(
            row['recordingId'], row['locationId'], row['frameRate'], row['speedLimit'],
            row['numTracks'], row['numVehicles'], row['numVRUs']
        )
        records_list.append(record)

    # Create a list of TrackMeta objects from raw data
    tracks_meta_list = []
    for _, row in raw_data_tracks_meta.iterrows():
        track_meta = TrackMeta(
            row['recordingId'], row['trackId'], row['initialFrame'], row['finalFrame'],
            row['numFrames'], row['tracked_object_width'], row['tracked_object_length'], row['class']
        )
        tracks_meta_list.append(track_meta)

    # Create a list of Trajectory objects from raw trajectory data
    trajectory_point_list = []
    for _, row in merged_data.iterrows():
        trajectory_point = Trajectory_point(
            row['recordingId'], row['trackId'], row['frame'], row['trackLifetime'],
            row['xCenter'], row['yCenter'], row['heading'], row['object_width'],
            row['object_length'], row['xVelocity'], row['yVelocity'],
            row['xAcceleration'], row['yAcceleration'], row['lonVelocity'],
            row['latVelocity'], row['lonAcceleration'], row['latAcceleration']
        )
        trajectory_point_list.append(trajectory_point)

    return locations_list, times_list, object_classes_list, records_list, tracks_meta_list, trajectory_point_list