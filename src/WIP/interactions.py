import pandas as pd
import numpy as np

# Charger votre dataset ici, remplacez "votre_dataset.csv" par le chemin de votre fichier CSV
trajectories_dataset = pd.read_csv("votre_dataset.csv")

def translate(x, y, angle, distance_to_translate=10):
    angle = np.radians(angle)
    return pd.DataFrame({
        'xCenter': x + distance_to_translate * np.cos(angle),
        'yCenter': y + distance_to_translate * np.sin(angle)
    })

def get_sens(angle1, angle2):
    angle = np.abs(angle1 - angle2)
    if angle > 180:
        angle = angle - 180
    if 10 < angle < 170:
        return "perp"
    else:
        return "id"

def get_stop_distance(speed):
    return speed + (speed / 10) ** 2

def get_speed(x_speed, y_speed):
    return np.sqrt(x_speed ** 2 + y_speed ** 2)

def get_view_field_points(t_id, f, number_of_points=10):
    angles_plus = trajectories_dataset[(trajectories_dataset['trackId'] == t_id) & (trajectories_dataset['frame'] == f)]['heading'].values + 60
    angles_minus = trajectories_dataset[(trajectories_dataset['trackId'] == t_id) & (trajectories_dataset['frame'] == f)]['heading'].values - 60

    view_field_plus = pd.concat([
        translate(
            x=trajectories_dataset[(trajectories_dataset['trackId'] == t_id) & (trajectories_dataset['frame'] == f)]['xCenter'].values,
            y=trajectories_dataset[(trajectories_dataset['trackId'] == t_id) & (trajectories_dataset['frame'] == f)]['yCenter'].values,
            angle=angle,
            distance_to_translate=d
        )
        for d, angle in zip(range(number_of_points * 10, 0, -10), angles_plus)
    ])

    view_field_minus = pd.concat([
        translate(
            x=trajectories_dataset[(trajectories_dataset['trackId'] == t_id) & (trajectories_dataset['frame'] == f)]['xCenter'].values,
            y=trajectories_dataset[(trajectories_dataset['trackId'] == t_id) & (trajectories_dataset['frame'] == f)]['yCenter'].values,
            angle=angle,
            distance_to_translate=d
        )
        for d, angle in zip(range(number_of_points * 10, 0, -10), angles_minus)
    ])

    view_field = pd.concat([view_field_plus, trajectories_dataset[(trajectories_dataset['trackId'] == t_id) & (trajectories_dataset['frame'] == f)][['xCenter', 'yCenter']]])
    view_field = pd.concat([view_field, view_field_minus])
    view_field = pd.concat([view_field, view_field.iloc[[0]]])

    return view_field

# Exemple d'utilisation :
t_id_exemple = 1  # Remplacez par l'ID de la piste souhaitée
f_exemple = 10    # Remplacez par le numéro de frame souhaité
number_of_points_exemple = 10

resultat = get_view_field_points(t_id_exemple, f_exemple, number_of_points_exemple)
print(resultat)

def get_type_of_interaction(id1, id2, frame, stop_distance, real_distance, sens):
    # Définition de l'interaction
    interaction = "Pas d'interaction"
    if real_distance > stop_distance:
        # Pas d'interactions
        pass
    elif real_distance > stop_distance / 2:
        if sens in ["perp"]:
            interaction = "Inconfort"
        else:
            # Pas d'interactions
            pass
    else:
        if sens in ["id"]:
            interaction = "Inconfort"
        else:
            interaction = "Conflit"
    
    return pd.DataFrame({
        'trackId1': id1,
        'trackId2': id2,
        'frame': frame,
        'interaction': interaction,
        'stopDistance': stop_distance,
        'realDistance': real_distance,
        'sens': sens
    }, index=[0])

# Exemple d'utilisation :
id1_exemple = 1  # Remplacez par l'ID de la première piste
id2_exemple = 2  # Remplacez par l'ID de la deuxième piste
frame_exemple = 10  # Remplacez par le numéro de frame
stop_distance_exemple = 5.0  # Remplacez par la distance d'arrêt
real_distance_exemple = 3.0  # Remplacez par la distance réelle
sens_exemple = "perp"  # Remplacez par le type de sens

resultat = get_type_of_interaction(id1_exemple, id2_exemple, frame_exemple, stop_distance_exemple, real_distance_exemple, sens_exemple)
print(resultat)

import numpy as np
from shapely.geometry import Point, Polygon

def get_interactions_point_frame(t_id, f, dataset):
    # On détermine le champ de vision
    shape_view_field = get_view_field_points(t_id, f)

    # Sélection des tracks dans le champ de vision et présents sur la frame
    ids = dataset[(dataset['frame'] == f) & (dataset['trackId'] != t_id)]['trackId'].unique()
    ids_in_view_field = ids[np.logical_and(
        point_in_polygon(
            dataset[(dataset['frame'] == f) & (dataset['trackId'] != t_id)]['xCenter'],
            dataset[(dataset['frame'] == f) & (dataset['trackId'] != t_id)]['yCenter'],
            shape_view_field[:, 0],
            shape_view_field[:, 1]
        ),
        True
    )]

    if len(ids_in_view_field) != 0:  # Skip si vide
        # Recherche de la distance d'arrêt
        speed = get_speed(
            dataset[(dataset['trackId'] == t_id) & (dataset['frame'] == f)]['xVelocity'].values[0],
            dataset[(dataset['trackId'] == t_id) & (dataset['frame'] == f)]['yVelocity'].values[0]
        )
        stop_distance = get_stop_distance(speed)
        stop_distance = max(stop_distance, 2)

        this_interaction = [
            get_type_of_interaction(
                t_id, this_id, f,
                stop_distance,
                np.linalg.norm(dataset[(dataset['trackId'] == t_id) & (dataset['frame'] == f)][['xCenter', 'yCenter']].values -
                               dataset[(dataset['trackId'] == this_id) & (dataset['frame'] == f)][['xCenter', 'yCenter']].values),
                get_sens(
                    dataset[(dataset['trackId'] == t_id) & (dataset['frame'] == f)]['heading'].values[0],
                    dataset[(dataset['trackId'] == this_id) & (dataset['frame'] == f)]['heading'].values[0]
                )
            )
            for this_id in ids_in_view_field
        ]

        return this_interaction
    else:
        return None

# Exemple d'utilisation :
t_id_exemple = 1  # Remplacez par l'ID de la piste souhaitée
f_exemple = 10    # Remplacez par le numéro de frame souhaité

resultat = get_interactions_point_frame(t_id_exemple, f_exemple, dataset)
print(resultat)

def get_all_interaction_of_frame(f, dataset):
    return [
        get_interactions_point_frame(x, f, dataset)
        for x in dataset[(dataset['frame'] == f)]['trackId'].unique()
    ]

def get_all_interactions_of_recording(r_id, dataset):
    dataset_recording = dataset[dataset['recordingId'] == r_id]
    unique_frames = dataset_recording['frame'].unique()

    interactions = [
        get_all_interaction_of_frame(x, dataset_recording)
        for x in unique_frames[unique_frames > 1]  # On applique uniquement aux frames où il y a plus d'une trajectoire
    ]

    return interactions

# Exemple d'utilisation :
recording_id_exemple = "XX"  # Remplacez par l'ID d'enregistrement souhaité

resultat = get_all_interactions_of_recording(recording_id_exemple, dataset)
print(resultat)
